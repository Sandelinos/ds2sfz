#!/usr/bin/env python3
import sys
import os
import xml.etree.ElementTree as ET
import math

sfz = ""
ignored_attributes = ['tags', 'loopCrossfadeMode', 'name']
unimplemented_attributes = []
sample_rate = 44100
red = '\033[0;31m'
nc = '\033[0m'

def parse_volume(value):
	if 'dB' in value:
		f = float(value.replace('dB', ''))
		return min(max(f, -144), 6)
	else:
		f = min(max(float(value), 0), 1)
		if f == 0:
			return -144
		else:
			return max(math.log(f, 10**0.05), -144)

def update_unimplemented_attributes(attribute):
	if attribute not in ignored_attributes:
		if attribute not in unimplemented_attributes:
			unimplemented_attributes.append(attribute)

if len(sys.argv) < 2:
	print('Usage: ' + sys.argv[0] + ' <file.dspreset>')
	sys.exit(0)
elif not os.path.isfile(sys.argv[1]):
	print(red + sys.argv[1] + ' is not a file' + nc)
	sys.exit(1)

try:
	tree = ET.parse(sys.argv[1])
except ET.ParseError as error:
	print(red + 'Parsing dspreset failed:' + nc, error)
	sys.exit(1)

root = tree.getroot()
groups_element = root.find('./groups')
groups = root.findall('./groups/group')

prompt = "Select group to convert\n"
for i, group in enumerate(groups):
	prompt += str(i + 1) + ". "
	for attribname, attrib in group.attrib.items():
		prompt += attribname + ": " + attrib + ", "
	prompt += "\n"
print(prompt)

choice = int(input(f"group (1-{len(groups)}): "))
chosen_group = groups[choice - 1]


global_attributes = {
	'volume':       'volume',
	"ampVelTrack":  "amp_veltrack",
	"attack":       "ampeg_attack",
	"decay":        "ampeg_decay",
	"sustain":      "ampeg_sustain",
	"release":      "ampeg_release"
}

def parse_global_attribute(key, value):
	if key in ["ampVelTrack", "sustain"]:
		return str(int(float(value) * 100))
	elif key == 'volume':
		return str(parse_volume(value))
	else:
		return value

sfz += "<global>"
for key in groups_element.attrib.keys():
	if key in global_attributes.keys():
		value = parse_global_attribute(key, groups_element.attrib[key])
		sfz += " " + global_attributes[key] + "=" + value
	else:
		update_unimplemented_attributes(key)
sfz += "\n"

sfz += "<group>"
for key in chosen_group.attrib.keys():
	if key in global_attributes.keys():
		value = parse_global_attribute(key, chosen_group.attrib[key])
		sfz += " " + global_attributes[key] + "=" + value
	else:
		update_unimplemented_attributes(key)
sfz += "\n"

region_attributes = {
	"path":         "sample",
	"start":        "offset",
	"end":          "end",
	'volume':       'volume',
	"rootNote":     "pitch_keycenter",
	"loNote":       "lokey",
	"hiNote":       "hikey",
	"loVel":        "lovel",
	"hiVel":        "hivel",
	"loopEnabled":  "loop_mode",
	"loopStart":    "loop_start",
	"loopEnd":      "loop_end",
	"loopCrossfade":"loop_crossfade"
}

def parse_region_attribute(key, value):
	if key == "loopEnabled":
		if bool(value) == True:
			return "loop_continuous"
		else:
			return "no_loop"
	elif key == "loopCrossfade":
		return str(float(value) / sample_rate)
	elif key == 'volume':
		return str(parse_volume(value))
	else:
		return value

for child in chosen_group:
	if child.tag == "sample":
		sfz += "<region>"
		for key in child.keys():
			if key in region_attributes.keys():
				value = parse_region_attribute(key, child.attrib[key])
				sfz += " " + region_attributes[key] + "=" + value
			else:
				update_unimplemented_attributes(key)

		sfz += "\n"

output_file = os.path.splitext(sys.argv[1])[0] + ".sfz"
with open(output_file, "w") as f:
	f.write(sfz)

print("SFZ written to " + output_file)
if len(unimplemented_attributes) > 0:
	print('Unimplemented attributes found: ', unimplemented_attributes)
