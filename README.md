# ds2sfz

This program converts Decent Sampler libraries into SFZ libraries.

It currently only works with `.dspreset` libraries so if you want to convert a
`.dslibrary` you'll need to unzip it first.

## Usage
```
$ python3 ds2sfz.py ~/samples/instrument.dspreset
Select group to convert
1. tags: normal,di, volume: 0.5,
2. tags: normal,mic, volume: 0.5,
3. name: DI, tags: vibrato,di, volume: 0.5,
4. name: Condenser, tags: vibrato,mic, volume: 0.5,

group (1-4): 3
SFZ written to /home/sandelinos/samples/instrument.sfz
```

## Implemented attributes

### \<group\>, \<groups\>

| Decent Sampler    | SFZ           |
|-------------------|---------------|
| volume            | volume        |
| ampVelTrack       | amp_veltrack  |
| attack            | ampeg_attack  |
| decay             | ampeg_decay   |
| sustain           | ampeg_sustain |
| release           | ampeg_release |

### \<sample\>

| Decent Sampler    | SFZ               |
|-------------------|-------------------|
| path              | sample            |
| start             | offset            |
| end               | end               |
| volume            | volume            |
| rootNote          | pitch_keycenter   |
| loNote            | lokey             |
| hiNote            | hikey             |
| loVel             | lovel             |
| hiVel             | hivel             |
| loopEnabled       | loop_mode         |
| loopStart         | loop_start        |
| loopEnd           | loop_end          |
| loopCrossfade     | loop_crossfade    |

## todo
* `tuning`
* sample rate detection
* `.dslibrary` extraction
